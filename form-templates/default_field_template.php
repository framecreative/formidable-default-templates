<?php
/*
  *
  * Default Field Template
  *
  *
  *
  */ ?>
<div id="frm_field_[id]_container" class="form_field">
	<div class="form_field_label [error_class]">
		<label for=field_[key] class="t-bold">[field_name]</label>
		[if error]<div class="form_field_error">[error]</div>[/if error]
	</div>
	<div class="form_field_main [required_class]">
		[input]
	</div>
	[if description]<div class="frm_description">[description]</div>[/if description]
</div>