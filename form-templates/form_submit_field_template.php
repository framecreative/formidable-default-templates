<?php
/*
  * Submit Field Template
  *
  * Changed to use a button
  *
  */ ?>
<div class="frm_submit form_footer">
	[if back_button]<input type="button" value="[back_label]" name="frm_prev_page" formnovalidate="formnovalidate" class="frm_prev_page" [back_hook] />[/if back_button]
	<button class="button" type="submit" ><span class="t-heading" [button_action] >[button_label]</button>
	<img class="frm_ajax_loading form_ajax_loader" src="[frmurl]/images/ajax_loader.gif" alt="Sending"/>
	[if save_draft]<a href="#" class="frm_save_draft" [draft_hook]>[draft_label]</a>[/if save_draft]
	<!-- [button_action] -->
</div>
