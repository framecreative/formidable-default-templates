<?php

/*
Plugin Name: Formidable Default Templates
Plugin URI: https://bitbucket.org/framecreative/formidable-default-templates
Description: Provides a template hierarchy so themes and this plugin can override the default Formidable HTML
Version: 1.2.1
Author: Alex Bishop / Frame Creative
Author URI: http://framecreative.com.au
License: GPL2
Bitbucket Plugin URI: https://bitbucket.org/framecreative/formidable-default-templates
Bitbucker Branch: release
*/

class Formidable_Default_Templates {

	/**
	 * @var
	 */
	protected static $version;


	/**
	 * Setup Hooks
	 *
	 * Adds filters and actions to the WordPress instance
	 *
	 * Checks if the hook already exists as a weak attempt
	 * at being an idempotent function
	 *
	 * Removes the need to enforce a singleton pattern
	 * on this class as any number of the classes can be
	 * instantiated without having side effects, though you
	 * shouldn't have any reason to.
	 *
	 * No explicit confirmation the hook succeeds
	 *
	 */
	public static function setup_hooks() {

		if ( ! has_filter( 'frm_form_options_before_update', 'Formidable_Default_Templates::replace_default_settings_html'  ) ) {

			add_filter( 'frm_form_options_before_update', 'Formidable_Default_Templates::replace_default_settings_html', 10, 2);

		}

		if ( ! has_filter( 'frm_custom_html', 'Formidable_Default_Templates::replace_default_field_html' ) ) {

			add_filter('frm_custom_html', 'Formidable_Default_Templates::replace_default_field_html' , 20, 2 );

		}

		if ( ! has_filter( 'frm_field_classes', 'Formidable_Default_Templates::replace_field_input_classes' ) ) {

			add_filter('frm_field_classes', 'Formidable_Default_Templates::replace_field_input_classes' , 20, 2 );

		}

	}

	/**
	 *
	 */
	public static function get_current_form_action() {

		if( !empty( $_REQUEST['frm_action'] ) ) {

			// Lets be paranoid with our inputs

			$input = preg_replace("/[^a-zA-Z0-9_-]+/", "", $_REQUEST['frm_action'] );

			return apply_filters( 'fdt_current_form_action', $input );

		}

	}


	/**
	 *
	 * We check for a new template
	 *
	 * Hierarchy:
	 *
	 * Child Theme: [field_name]_template.php
	 * Parent Theme: [field_name]_template.php
	 * Plugin: [field_name]_template.php
	 *
	 * Child Theme: default_field_template.php
	 * Parent Theme: default_field_template.php
	 * Plugin: default_field_template.php
	 *
	 * due to the paths we traverse this isn't
	 * super performant, but we're ONLY running on
	 * new form field creation or new form creation
	 * so the perf issues will never impact the front
	 * end
	 *
	 * @param $field Object
	 *
	 * @return string $new_default_form_markup
	 */

	public static function get_template_or_default( $default_html, $field_tag, $context = '' ){

		$template = self::locate_template( $field_tag . '_field_template.php' );

		if ( ! $template && $context === 'new_field' ) {
			$template = self::locate_template( 'default_field_template.php' );
		}

		if ( ! $template or ! file_exists( $template ) ) {
			return $default_html;
		}

		ob_start();
			load_template( $template );

		$new_markup = ob_get_clean();

		return apply_filters( 'fdt_field_markup_' . $field_tag, $new_markup, $context );

	}

	/**
	 * Locate Template
	 *
	 * Heavily inspired by WooThemes
	 *
	 * @param $template_name
	 * @param string $template_path
	 * @param string $default_path
	 *
	 * @return string - The path to the template
	 */
	public static function locate_template( $template_name, $template_path = '', $default_path = '' ) {
		if ( ! $template_path ) {
			$template_path =  ( defined( 'FDT_TEMPLATE_PATH') ) ? FDT_TEMPLATE_PATH : 'form-templates' ;
		}

		if ( ! $default_path ) {
			$default_path = plugin_dir_path( __FILE__ ) . 'form-templates/';
		}

		// Theme overrides our included plugin templates
		// Uses locate_template so child theme compatible

		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name
			)
		);

		// Get default template/
		if ( ! $template ) {
			$template = $default_path . $template_name;
		}

		if (! file_exists( $template ) ) {
			$template = false;
		}

		// Return what we found.
		return apply_filters( 'fdt_locate_template', $template, $template_name, $template_path );

	}

	/**
	 *
	 */
	public static function replace_default_field_html( $default_html, $field_type ) {

		$context  = 'new_field';

		return self::get_template_or_default( $default_html, $field_type, $context );

	}

	/**
	 *
	 */
	public static function replace_default_settings_html( $options, $values) {

		if ( self::get_current_form_action() !== 'new' ) {
			return $options;
		}

		$options['before_html'] = self::get_template_or_default( $options['before_html'], 'form_before' );
		$options['after_html'] = self::get_template_or_default( $options['after_html'], 'form_after' );
		$options['submit_html'] = self::get_template_or_default( $options['submit_html'], 'form_submit' );

		return $options;
	}

	public static function replace_field_input_classes( $classes, $field ) {

		$additional_classes = array(
			'text'      => 'form_textinput',
			'email'     => 'form_textinput',
			'textarea'  => 'form_textarea',

		);

		$replaced_classes = array();

		$additional_classes = apply_filters( 'fdt_inputs_additional_classes', $additional_classes );

		$replaced_classes = apply_filters( 'fdt_inputs_replaced_classes', $replaced_classes );

		if ( ! empty( $replaced_classes) ) {

		}

		if ( ! empty( $additional_classes ) && isset( $additional_classes[ $field['type'] ] ) ) {

			$field_classes = $additional_classes[ $field['type'] ];

			if ( is_string( $field_classes ) ) {
				return $classes . " " . $field_classes;
			}

			if ( is_array( $field_classes ) ) {
				return $classes . implode( " ", $field_classes );
			}
		}

		return $classes;

	}



}

add_action( 'init', 'Formidable_Default_Templates::setup_hooks' );